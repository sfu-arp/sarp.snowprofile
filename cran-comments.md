## Update description

The new package version depends on R >= 4.2, because R 3.x breaks the functionality in weird ways.
Other updates are minor.

## Test environments

* local Linux Mint 19.1 install (Ubuntu 18.04), R 4.2.2
* devtools::check_win_devel()
* devtools::check_rhub()


## R CMD check result

There were no ERRORs and no WARNINGs and no NOTEs.


## Downstream dependencies
There are two downstream dependency from the same author team, `sarp.snowprofile.alignment` 
and `sarp.snowprofile.pyface`, which produce *no* errors with the new upstream changes. 
